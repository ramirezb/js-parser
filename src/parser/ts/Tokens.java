package parser.ts;

/**
 * Lista de tokens reconocidos en el lexico.
 * @author I. Sanchez, A. Canser, J. Cano.
 */
public enum Tokens {
	PALABRA,
	PUNTO_1, 
	PUNTO_2,
	PUNTO_C,
	COMILLA,
	PAR_ABR,
	PAR_CIE,
	COMMENT,
	
	__UNK__,
}
