package parser;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import parser.ts.Tokens;

/**
 * Logica del analizador lexico
 * @author I. Sanchez, A. Canser, J. Cano.
 */
public class Lexico {
	
	private List<Token> tokens;
	private FileReader testbed;
	private String text;
	
	public Lexico(String path) throws FileNotFoundException {
		this.tokens = new ArrayList<Token>();
		this.testbed = new FileReader(path);
		this.text = "";
	}
	
	public String parse() {
		
		toString();
		supressComments();
		
		String result = "";
		String delimiters = " ,;.:()\n\t\r";
		StringTokenizer tokenizer = new StringTokenizer(this.text, delimiters, true);
		while (tokenizer.hasMoreTokens())
		{
			String token = tokenizer.nextToken();
			if (!isDelimiter(token)) {
				this.tokens.add(new Token(getFromAutomaton(token), token));
				result += token + "\n";
			}
		}
		return result;
	}
	
	public void parseToFile(String pathToFile) throws FileNotFoundException{
		parse();
		PrintWriter out = new PrintWriter(pathToFile);
		for (Token token : tokens) {
			out.print(token.toString() + "\n");
		}
		out.close();
	}
	
	private void supressComments() {
		this.text = 
				this.text.replaceAll(RegExp.COMENTARIO," ");
	}

	private Tokens getFromAutomaton(String token) {
		// TODO mi reconocedor de palabras.
		Tokens result = Tokens.__UNK__;
		if (token.matches(RegExp.PALABRA))
			result = Tokens.PALABRA;
		else if (token.equals("(")){
			result = Tokens.PAR_ABR;
		}		
		else if (token.equals(".")){
			result = Tokens.PUNTO_1;
		}
		else if (token.equals(")")){
			result = Tokens.PAR_CIE;
		}
		return result;
	}

	private boolean isDelimiter(String token) {
		if (token.equals(" ") ||
				token.equals("\t") ||	
				token.equals("\n") ||	
				token.equals("\r")) 
			return true;
		return false;
	}
	
	public List<Token> getTokens() {
		return tokens;
	}
	
	public String toString(){
		BufferedReader br = new BufferedReader(testbed);
		try {
		    StringBuilder sb = new StringBuilder();
		    String line;
				line = br.readLine();
		    while (line != null) {
		        sb.append(line);
		        sb.append(System.lineSeparator());
		        line = br.readLine();
		    }
		    this.text = sb.toString();
		    br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		return this.text;
	}

	final class Token {
		private Tokens type;
		private String value;
		
		public Token(Tokens type, String value){
			this.type = type;
			this.value = value;
		}
		
		public Tokens getType() {
			return type;
		}
		public String getValue() {
			return value;
		}	
		
		public String toString () {
			//return this.type.name() + " ";
			return this.type.name() + "\t\t<" + this.value + ">";
		}
	}
	
	final class RegExp {

	    public static final String COMENTARIO = "(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)";
	    public static final String PALABRA = ".*[a-zA-Z]+.*";

	}
	
}
