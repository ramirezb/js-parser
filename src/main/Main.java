package main;

import java.io.FileNotFoundException;

import parser.*;

public class Main {
	
	private static final String app = "PDL JS-Parser v0.1 @ ";

	public static void main(String[] args) {
		System.out.println(app + "Initializing");
		
		String testpath = "files/testbed/mi-libro-1.js";
		String lexicopath = "files/io/salida del lexico.txt";
		
		try {
			poprintln("Running lexical analyzer...");
			
			Lexico miLexico = new Lexico(testpath);
			//poprintln("\n" + miLexico.toString());
			//poprintln("\n" + miLexico.parse());
			
			miLexico.parseToFile(lexicopath);
			
		} catch (FileNotFoundException e) {
			peprintln(Main.class, "Fichero " + testpath + " no encontrado");
		}
		
		System.out.println(app + "Done");
	}
	
	public final static void poprintln (String text) {
		System.out.println(app + text);
	}
	
	public final static void peprintln (Class<?> clase, String text) {
		System.err.println(Main.app + clase.getName() + ": " + text);
	}

}